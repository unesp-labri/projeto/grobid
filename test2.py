from posixpath import dirname
from grobid_client.grobid_client import GrobidClient
import os
import re

# Initialize the GROBID client
client = GrobidClient(config_path="./grobid_client_python/config.json")

# Specify the PDF file you want to process
entrada = "/home/labri_malakchefrid/Documentos/codigos/grobid/Test-pdf/1690-06012021.pdf"
saida ="/home/labri_malakchefrid/Documentos/codigos/grobid/Test-pdf/1690-06012021.xml"

client.process("processReferences", entrada, output=saida,consolidate_citations=True,include_raw_citations=True, force=True)